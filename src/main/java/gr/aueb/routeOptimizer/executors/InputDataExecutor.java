package gr.aueb.routeOptimizer.executors;

import gr.aueb.routeOptimizer.models.VisitLocation;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class InputDataExecutor {

    // Input of the problem
    // Generates a list of customers (VisitLocation)
    public List<VisitLocation> createAllNodesAndCustomerLists(int numberOfCustomers) {
        //Create the list with the customers
        List<VisitLocation> customers = new ArrayList<>();
        int birthday = 212242; // if your bday is on 9 feb 1998
        Random ran = new Random(birthday);
        // create the starting point
        VisitLocation depot = getStartingPoint();
        // add it as the first item of the list of customers
        customers.add(depot);
        // create as many customers as requested in main method in RouteOptimizer (default 100)
        for (int i = 0; i < numberOfCustomers; i++) {
            // create a new customer and init its fields
            VisitLocation visitLocation = new VisitLocation();
            visitLocation.setLatitude(ran.nextInt(100));
            visitLocation.setLongitude(ran.nextInt(100));
            visitLocation.setProducts(100 * (1 + ran.nextInt(5)));
            visitLocation.setServiceTime(0.25);
            // add the customer to the list
            customers.add(visitLocation);
        }
        // give an id to each customer (based on its index in the list)
        for (int i = 0; i < customers.size(); i++) {
            customers.get(i).setClientId(i);
        }
        return customers;
    }

    // method to create the starting point of each route
    private VisitLocation getStartingPoint() {
        VisitLocation depot = new VisitLocation();
        depot.setLatitude(50);
        depot.setLongitude(50);
        depot.setProducts(0);
        return depot;
    }

    // Create a 2D array with the distances between all customers
    public List<List<Double>> createDistanceMatrix(List<VisitLocation> visitLocations) {
        List<List<Double>> distanceMatrix = new ArrayList<>();
        // execute two for loops to calculate the distances between all customers
        for (int i = 0; i < visitLocations.size(); i++) {
            // get the starting customer
            VisitLocation visitLocationFrom = visitLocations.get(i);
            // create a list with the distances for this customer
            List<Double> distances = new ArrayList<>();
            for (VisitLocation visitLocationTo : visitLocations) {
                // get the destination (to)
                // get the x distance
                Integer latitudeDist = visitLocationFrom.getLatitude() - visitLocationTo.getLatitude();
                // get the y distance
                Integer longitudeDist = visitLocationFrom.getLongitude() - visitLocationTo.getLongitude();
                // get the euclidean distance between the two points (customers)
                Double distance = Math.sqrt((latitudeDist * latitudeDist) + (longitudeDist * longitudeDist));
                // add the distance to the list of distances
                distances.add(distance);
            }
            // add the list of distances for this client to the 2D array
            distanceMatrix.add(distances);
        }
        return distanceMatrix;
    }
}
