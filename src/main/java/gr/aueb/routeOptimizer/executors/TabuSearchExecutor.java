package gr.aueb.routeOptimizer.executors;

import gr.aueb.routeOptimizer.models.*;
import gr.aueb.routeOptimizer.utils.ResultsWriter;
import gr.aueb.routeOptimizer.utils.SolutionDrawer;
import gr.aueb.routeOptimizer.utils.Utils;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

// RelocationMove : get a customer from route 1 and insert it in route 2
// SwapMove : get a customer from route 1 and insert it in route 2 and also get the customer in route 2 where customer 1 will be inserted
// and add him to route 1
public class TabuSearchExecutor {

    // set the datatype to your liking
    // i guess this can hold origin and destination ids
    // super class Move is created so as to be able to add both 
    //RelocationMove and SwapMove objects in this list
    private LinkedList<Move> tabuList = new LinkedList<>();

    // 2D array with the distances between customers - it is produced by InputDataExecutor
    private List<List<Double>> distanceMatrix = new ArrayList<>();

    // Question 4 - Tabu search
    public void tabuSearch(Solution currentSolution, List<VisitLocation> allNodes, List<List<Double>> distanceMatrix) {
        System.out.println("============= Tabu Search Algorithm =================");
        this.distanceMatrix = distanceMatrix;
        // start with the BPP solution as best solution - create a new object
        // keep the best solution in Tabu Search
        Solution bestSolutionThroughTabuSearch = Utils.cloneSolution(currentSolution);
        // instanciate RelocationMove and SwapMove objects
        RelocationMove rm = new RelocationMove();
        SwapMove sm = new SwapMove();
        // initialCost = the cost of the BPP algorithm solution
        double initialCost = bestSolutionThroughTabuSearch.getTotalCost();
        // counts how many times TabuSearch is executed
        int repetitionCounter = 0;
        // TABU_REPETITIONS should be 10000 (see Utils)
        while (repetitionCounter < Utils.TABU_REPETITIONS) {
            // set the move cost of rm and sm object to the max possible Double value
            initializeOperators(rm, sm);
            // operatorType is a random number getting values 0 or 1
            int operatorType = Utils.OPERATOR_TYPE;
            // for operator type 0 we produce RelocationMove to find a better solution
            if (operatorType == 0) {
                FindBestRelocationMove(rm, bestSolutionThroughTabuSearch);
            } else if (operatorType == 1) {
                // for operator type 1 we produce SwapMove object to find a better solution
                FindBestSwapMove(sm, bestSolutionThroughTabuSearch);
            }
            // when we find a local minimum (no better solution can be produced) break the while loop
            if (localOptimumHasBeenReached(operatorType, rm, sm)) {
                System.out.println("Local optimum has been encountered -- stopping optimization.");
                break;
            }
            //Apply move
            ApplyMove(operatorType, rm, sm, bestSolutionThroughTabuSearch);
            testSolution(bestSolutionThroughTabuSearch);
            // create an image of the solution
             SolutionDrawer.drawRoutes(allNodes, bestSolutionThroughTabuSearch,
                    Utils.TABU_RESULTS_IMAGE + repetitionCounter);
             // update the repetition counter
            repetitionCounter++;
            // print the best solution after some iterations
            if (repetitionCounter % 250 == 0 && repetitionCounter > 0) {
                for (TruckRoute tr : bestSolutionThroughTabuSearch.getRoutes()) System.out.println(tr);
                System.out.println("Tabu cost for the  " + repetitionCounter + "-th iteration: " + bestSolutionThroughTabuSearch.getTotalCost());
            }
        }
        // write the final best solution in a file in json format
        ResultsWriter.writeResults(bestSolutionThroughTabuSearch, Utils.TABU_RESULTS_FILE);
        // print the initial cost from the BPP algorithm
        System.out.println("Cost prior to optimization: " + initialCost);
        // print the number of iterations and the new total cost
        System.out.println("Tabu cost for " + Utils.TABU_REPETITIONS + " iterations: " + bestSolutionThroughTabuSearch.getTotalCost());
        System.out.println("============= Finished Tabu Search Algorithm =================");
    }

    // init the two Move objects (relocation and swap) 
    // init their moveCost field with the max double value
    private void initializeOperators(RelocationMove rm, SwapMove sm) {
        rm.setMoveCost(Double.MAX_VALUE);
        sm.setMoveCost(Double.MAX_VALUE);
    }

    // calculate the best relocation move
    private void FindBestRelocationMove(RelocationMove rm, Solution sol) {
        // get the route list from the Solution object
        List<TruckRoute> routes = sol.getRoutes();
        // iterate the list to get the origin route index
        for (int originRouteIndex = 0; originRouteIndex < routes.size(); originRouteIndex++) {
            TruckRoute rt1 = routes.get(originRouteIndex);
            // iterate to get the target route index
            for (int targetRouteIndex = 0; targetRouteIndex < routes.size(); targetRouteIndex++) {
                TruckRoute rt2 = routes.get(targetRouteIndex);
                // iterate the clientsToVisitList of the origin route
                for (int originNodeIndex = 1; originNodeIndex < rt1.getClientsToVisit().size() - 1; originNodeIndex++) {
                    // iterate the clientsToVisitList of the target route
                    for (int targetNodeIndex = 0; targetNodeIndex < rt2.getClientsToVisit().size() - 1; targetNodeIndex++) {
                        // if the origin route is the same with the target route
                        // and if the target client is the same as the origin client or the target client is the previous of the origin client
                        // do nothing
                        if (originRouteIndex == targetRouteIndex &&
                                (targetNodeIndex == originNodeIndex || targetNodeIndex == originNodeIndex - 1)) {
                            continue;
                        }
                        // get the previous origin client
                        VisitLocation a = rt1.getClientsToVisit().get(originNodeIndex - 1);
                        // get the origin client
                        VisitLocation b = rt1.getClientsToVisit().get(originNodeIndex);
                        // get the origin next client
                        VisitLocation c = rt1.getClientsToVisit().get(originNodeIndex + 1);
                        // get the target client
                        VisitLocation insPoint1 = rt2.getClientsToVisit().get(targetNodeIndex);
                        // get the target next client
                        VisitLocation insPoint2 = rt2.getClientsToVisit().get(targetNodeIndex + 1);

                        //capacity constraints
                        // if the two routes (origin and target) are different
                        if (originRouteIndex != targetRouteIndex) {
                            // if the filled space of the target route plus the products of the origin client are more than
                            // the truck's capacity, do nothing
                            if (rt2.getFilledSpace() + b.getProducts() > TruckRoute.capacity) {
                                continue;
                            }
                        }
                        // calculate the distance between a,c (after b is removed c will be the next customer after a)
                        // add the distance between the target client and the origin client and the distance between the
                        // origin client and the target next client (the order will be insPoint1 --> b --> insPoint2)
                        double costAdded = distanceMatrix.get(a.getClientId()).get(c.getClientId())
                                + distanceMatrix.get(insPoint1.getClientId()).get(b.getClientId())
                                + distanceMatrix.get(b.getClientId()).get(insPoint2.getClientId());
                        // calculate the cost of a --> b --> c and insPoint1 --> insPoint2
                        double costRemoved = distanceMatrix.get(a.getClientId()).get(b.getClientId())
                                + distanceMatrix.get(b.getClientId()).get(c.getClientId())
                                + distanceMatrix.get(insPoint1.getClientId()).get(insPoint2.getClientId());
                        // calculate the cost of this move
                        double moveCost = costAdded - costRemoved;

                        // calculate the cost of changing the origin route a --> c cost minus a --> b --> c cost
                        double costChangeOriginRoute = distanceMatrix.get(a.getClientId()).get(c.getClientId()) -
                                (distanceMatrix.get(a.getClientId()).get(b.getClientId())
                                        + distanceMatrix.get(b.getClientId()).get(c.getClientId()));
                        // calculate the cost of changing the target route : inspoint1 --> b --> inspoint2 cost minus
                        // inspoint1 --> inspoint2 cost
                        double costChangeTargetRoute = distanceMatrix.get(insPoint1.getClientId()).get(b.getClientId())
                                + distanceMatrix.get(b.getClientId()).get(insPoint2.getClientId())
                                - distanceMatrix.get(insPoint1.getClientId()).get(insPoint2.getClientId());
                        // route change cost
                        double totalObjectiveChange = costChangeOriginRoute + costChangeTargetRoute;
                        // update the RelocationMove object
                        RelocationMove testRm = new RelocationMove(originRouteIndex, targetRouteIndex,
                                originNodeIndex, targetNodeIndex, moveCost);
                        // check if the move is tabu (is in the LinkedList?)
                        if (moveIsTabu(testRm)) {
                            // if the move is in the list then continue
                            continue;
                        }
                        // the move is not Tabu so store the new best relocation move
                        StoreBestRelocationMove(originRouteIndex, targetRouteIndex, originNodeIndex, targetNodeIndex, moveCost, rm);
                    }
                }
            }
        }
    }
    // Store the best relocation move
    private void StoreBestRelocationMove(int originRouteIndex, int targetRouteIndex, int originNodeIndex, int targetNodeIndex,
                                         double moveCost, RelocationMove rm) {
        // if the cost is less than the current cost of the Relocation move - found a better relocation move
        if (moveCost < rm.getMoveCost()) {
            // update the RelocationMove fields (indexes and move cost)
            rm.setOriginClientPosition(originNodeIndex);
            rm.setTargetClientPosition(targetNodeIndex);
            rm.setTargetRoutePosition(targetRouteIndex);
            rm.setOriginRoutePosition(originRouteIndex);
            rm.setMoveCost(moveCost);
        }
    }

    // find the best SwapMove
    private void FindBestSwapMove(SwapMove sm, Solution sol) {
        // get the route list from the solution object
        List<TruckRoute> routes = sol.getRoutes();
        // iterate the list to find the first route
        for (int firstRouteIndex = 0; firstRouteIndex < routes.size(); firstRouteIndex++) {
            TruckRoute rt1 = routes.get(firstRouteIndex);
            // iterate the list to find the second route
            for (int secondRouteIndex = firstRouteIndex; secondRouteIndex < routes.size(); secondRouteIndex++) {
                TruckRoute rt2 = routes.get(secondRouteIndex);
                // iterate the clientsToVisit of the first route
                for (int firstNodeIndex = 1; firstNodeIndex < rt1.getClientsToVisit().size() - 1; firstNodeIndex++) {
                    // get the index of the second customer node
                    int startOfSecondNodeIndex = 1;
                    // if first == second route get the next client in the list
                    if (rt1.equals(rt2)) {
                        startOfSecondNodeIndex = firstNodeIndex + 1;
                    }
                    // iterate the clientsToVisit of the second route starting from the second's client index
                    for (int secondNodeIndex = startOfSecondNodeIndex; secondNodeIndex < rt2.getClientsToVisit().size() - 1; secondNodeIndex++) {
                        // previous customer in the first route
                        VisitLocation a1 = rt1.getClientsToVisit().get(firstNodeIndex - 1);
                        // customer in the first route
                        VisitLocation b1 = rt1.getClientsToVisit().get(firstNodeIndex);
                        // next customer in the first route
                        VisitLocation c1 = rt1.getClientsToVisit().get(firstNodeIndex + 1);
                        // previous customer in the second route
                        VisitLocation a2 = rt2.getClientsToVisit().get(secondNodeIndex - 1);
                        // customer in the second route
                        VisitLocation b2 = rt2.getClientsToVisit().get(secondNodeIndex);
                        // next customer in the second route
                        VisitLocation c2 = rt2.getClientsToVisit().get(secondNodeIndex + 1);
                        // maximize the move cost
                        double moveCost = Double.MAX_VALUE;
                        // if we check the same route
                        if (rt1 == rt2) {
                            // if the first node client is the previous client of the second customer
                            if (firstNodeIndex == secondNodeIndex - 1) {
                                // calculate the cost to remove and to add a customer
                                double costRemoved = getCost(a1, b1, b2, c2);
                                double costAdded = getCost(a1, b2, b1, c2);
                                // find the move cost
                                moveCost = costAdded - costRemoved;
                                // create a new SwapMove
                                SwapMove testSm = new SwapMove(firstRouteIndex, secondRouteIndex, firstNodeIndex, secondNodeIndex, moveCost);
                                // check if this is a tabu move
                                if (moveIsTabu(testSm)) {
                                    continue;
                                }
                            } else {
                                // if the customers are not neighboring calculate the move cost
                                moveCost = getMoveCost(a1, b1, c1, a2, b2, c2);
                                // create a SwapMove object
                                SwapMove testSm = new SwapMove(firstRouteIndex, secondRouteIndex, firstNodeIndex, secondNodeIndex, moveCost);
                                // check for tabu move
                                if (moveIsTabu(testSm)) {
                                    continue;
                                }
                            }
                        } else { // if the routes are different
                            //capacity constraints - check the filled space and the products of the new customers
                            if (rt1.getFilledSpace() - b1.getProducts() + b2.getProducts() > TruckRoute.capacity) {
                                continue;
                            }
                            if (rt2.getFilledSpace() - b2.getProducts() + b1.getProducts() > TruckRoute.capacity) {
                                continue;
                            }
                            // calculate the move cost
                            moveCost = getMoveCost(a1, b1, c1, a2, b2, c2);
                            // create a SwapMove object
                            SwapMove testSm = new SwapMove(firstRouteIndex, secondRouteIndex, firstNodeIndex, secondNodeIndex, moveCost);
                            // check if it is a tabu move
                            if (moveIsTabu(testSm)) {
                                continue;
                            }
                        }
                        // if reached here the move is not tabu
                        // update the SwapMove
                        StoreBestSwapMove(firstRouteIndex, secondRouteIndex, firstNodeIndex, secondNodeIndex, moveCost, sm);
                    }
                }
            }
        }
    }

    // calculate the Swap move cost
    private double getMoveCost(VisitLocation a1, VisitLocation b1, VisitLocation c1, VisitLocation a2, VisitLocation b2, VisitLocation c2) {
        double moveCost;
        // a1 --> b1 --> c1 cost
        double costRemoved1 = distanceMatrix.get(a1.getClientId()).get(b1.getClientId())
                + distanceMatrix.get(b1.getClientId()).get(c1.getClientId());
        // a1 --> b2 --> c1 cost
        double costAdded1 = distanceMatrix.get(a1.getClientId()).get(b2.getClientId())
                + distanceMatrix.get(b2.getClientId()).get(c1.getClientId());
        // a2 --> b2 --> c2 cost
        double costRemoved2 = distanceMatrix.get(a2.getClientId()).get(b2.getClientId())
                + distanceMatrix.get(b2.getClientId()).get(c2.getClientId());
        // a2 --> b1 --> c2 cost
        double costAdded2 = distanceMatrix.get(a2.getClientId()).get(b1.getClientId())
                + distanceMatrix.get(b1.getClientId()).get(c2.getClientId());
        // calculate the move cost
        moveCost = costAdded1 + costAdded2 - (costRemoved1 + costRemoved2);
        return moveCost;
    }

    // calculate the cost a1 --> b1 --> b2 --> c2
    private double getCost(VisitLocation a1, VisitLocation b1, VisitLocation b2, VisitLocation c2) {
        return distanceMatrix.get(a1.getClientId()).get(b1.getClientId())
                + distanceMatrix.get(b1.getClientId()).get(b2.getClientId())
                + distanceMatrix.get(b2.getClientId()).get(c2.getClientId());
    }

    // if the new move cost of a SwapMove is better then update the indexes and the move cost of the SwapMove
    private void StoreBestSwapMove(int firstRouteIndex, int secondRouteIndex, int firstNodeIndex, int secondNodeIndex, double moveCost, SwapMove sm) {
        if (moveCost < sm.getMoveCost()) {
            sm.setFirstRoutePosition(firstRouteIndex);
            sm.setFirstClientPosition(firstNodeIndex);
            sm.setSecondRoutePosition(secondRouteIndex);
            sm.setSecondClientPosition(secondNodeIndex);
            sm.setMoveCost(moveCost);
        }
    }

    // execute the move
    private void ApplyMove(int operatorType, RelocationMove rm, SwapMove sm, Solution sol) {
        if (operatorType == 0) {
            ApplyRelocationMove(rm, sol);
        } else if (operatorType == 1) {
            ApplySwapMove(sm, sol);
        }
    }

    // execute a Relocation move
    private void ApplyRelocationMove(RelocationMove rm, Solution sol) {
        // if the cost is not updated from the max value
        if (rm.getMoveCost().equals(Double.MAX_VALUE)) {
            return;
        }
        // get the origin and target routes from the list (use the indexes of the RelocationMove object)
        TruckRoute originRoute = sol.getRoutes().get(rm.getOriginRoutePosition());
        TruckRoute targetRoute = sol.getRoutes().get(rm.getTargetRoutePosition());
        // get the customer to remove
        VisitLocation B = originRoute.getClientsToVisit().get(rm.getOriginClientPosition());
        // if the origin and target routes are the same
        if (originRoute == targetRoute) {
            // remove the customer from the list and add him in the target list in the target position
            originRoute.getClientsToVisit().remove(rm.getOriginClientPosition().intValue());
            if (rm.getOriginClientPosition() < rm.getTargetClientPosition()) {
                targetRoute.getClientsToVisit().add(rm.getTargetClientPosition(), B);
            } else {
                targetRoute.getClientsToVisit().add(rm.getTargetClientPosition() + 1, B);
            }
            // calculate the total distance
            originRoute.setTotalDistance(originRoute.getTotalDistance() + rm.getMoveCost());
        } else {
            // if the routes are different
            // get the previous and next origin customers
            VisitLocation A = originRoute.getClientsToVisit().get(rm.getOriginClientPosition() - 1);
            VisitLocation C = originRoute.getClientsToVisit().get(rm.getOriginClientPosition() + 1);
            // get the current target and next customers
            VisitLocation F = targetRoute.getClientsToVisit().get(rm.getTargetClientPosition());
            VisitLocation G = targetRoute.getClientsToVisit().get(rm.getTargetClientPosition() + 1);
            // caclulate the origin cost A-->C minus A-->B-->C
            double costChangeOrigin = distanceMatrix.get(A.getClientId()).get(C.getClientId())
                    - distanceMatrix.get(A.getClientId()).get(B.getClientId())
                    - distanceMatrix.get(B.getClientId()).get(C.getClientId());
            // calculate the target cost F-->B-->G minus F-->G
            double costChangeTarget = distanceMatrix.get(F.getClientId()).get(B.getClientId())
                    + distanceMatrix.get(B.getClientId()).get(G.getClientId())
                    - distanceMatrix.get(F.getClientId()).get(G.getClientId());
            // update the filled and remaining spaces of the two routes (since they are different)
            originRoute.setFilledSpace(originRoute.getFilledSpace() - B.getProducts());
            originRoute.setRemainingSpace(originRoute.getRemainingSpace() + B.getProducts());
            targetRoute.setFilledSpace(targetRoute.getFilledSpace() + B.getProducts());
            targetRoute.setRemainingSpace(targetRoute.getRemainingSpace() - B.getProducts());
            // udpate the totalDistance (totalcost) of the origin and target routes
            originRoute.setTotalDistance(originRoute.getTotalDistance() + costChangeOrigin);
            targetRoute.setTotalDistance(targetRoute.getTotalDistance() + costChangeTarget);
            // execute the Relocation move - remove the customer from the origin list 
            // and add the customer to the target list
            originRoute.getClientsToVisit().remove(rm.getOriginClientPosition().intValue());
            targetRoute.getClientsToVisit().add(rm.getTargetClientPosition() + 1, B);
        }
        // update the total cost of the solution
        sol.setTotalCost(sol.getTotalCost() + rm.getMoveCost());
    }

    // execute a swap move
    private void ApplySwapMove(SwapMove sm, Solution sol) {
        // do nothing if the SwapMove cost has the max double value
        if (sm.getMoveCost().equals(Double.MAX_VALUE)) {
            return;
        }
        // get the first and second routes
        TruckRoute firstRoute = sol.getRoutes().get(sm.getFirstRoutePosition());
        TruckRoute secondRoute = sol.getRoutes().get(sm.getSecondRoutePosition());

        // if the routes are the same
        if (firstRoute.equals(secondRoute)) {
            // if the firstClient is the previous of the second client
            if (sm.getFirstClientPosition().equals(sm.getSecondClientPosition() - 1)) {
                //get the two customers
                VisitLocation A = firstRoute.getClientsToVisit().get(sm.getFirstClientPosition());
                VisitLocation B = firstRoute.getClientsToVisit().get(sm.getFirstClientPosition() + 1);
                // move customer A after customer B
                firstRoute.getClientsToVisit().set(sm.getFirstClientPosition(), B);
                firstRoute.getClientsToVisit().set(sm.getFirstClientPosition() + 1, A);
            } else {
                // if the two customers are not neighboring
                // get the two clients based on the SwapMove indexes
                VisitLocation A = firstRoute.getClientsToVisit().get(sm.getFirstClientPosition());
                VisitLocation B = firstRoute.getClientsToVisit().get(sm.getSecondClientPosition());
                // add the customer B to the first index
                // add customer A to the second index 
                // i.e. swap the two clients
                firstRoute.getClientsToVisit().set(sm.getFirstClientPosition(), B);
                firstRoute.getClientsToVisit().set(sm.getSecondClientPosition(), A);
            }
            // calculate the new total distance of the route (first and second routes are the same)
            firstRoute.setTotalDistance(firstRoute.getTotalDistance() + sm.getMoveCost());
        } else {
            // if the routes are different
            //get the previous, current and next customers of the first route
            VisitLocation A = firstRoute.getClientsToVisit().get(sm.getFirstClientPosition() - 1);
            VisitLocation B = firstRoute.getClientsToVisit().get(sm.getFirstClientPosition());
            VisitLocation C = firstRoute.getClientsToVisit().get(sm.getFirstClientPosition() + 1);
            //get the previous, current and next customers of the second route
            VisitLocation E = secondRoute.getClientsToVisit().get(sm.getSecondClientPosition() - 1);
            VisitLocation F = secondRoute.getClientsToVisit().get(sm.getSecondClientPosition());
            VisitLocation G = secondRoute.getClientsToVisit().get(sm.getSecondClientPosition() + 1);

            // calculate the change cost : A-->F-->C minus A-->B-->C
            double costChangeFirstRoute = distanceMatrix.get(A.getClientId()).get(F.getClientId())
                    + distanceMatrix.get(F.getClientId()).get(C.getClientId())
                    - distanceMatrix.get(A.getClientId()).get(B.getClientId())
                    - distanceMatrix.get(B.getClientId()).get(C.getClientId());
            // calculate the change cost : E-->B-->G minus E-->F-->G
            double costChangeSecondRoute = distanceMatrix.get(E.getClientId()).get(B.getClientId())
                    + distanceMatrix.get(B.getClientId()).get(G.getClientId())
                    - distanceMatrix.get(E.getClientId()).get(F.getClientId())
                    - distanceMatrix.get(F.getClientId()).get(G.getClientId());
            // update the total distance of the two routes
            firstRoute.setTotalDistance(firstRoute.getTotalDistance() + costChangeFirstRoute);
            secondRoute.setTotalDistance(secondRoute.getTotalDistance() + costChangeSecondRoute);
            // update the filled and remaining space of the two routes
            firstRoute.setFilledSpace(firstRoute.getFilledSpace() + F.getProducts() - B.getProducts());
            firstRoute.setRemainingSpace(firstRoute.getRemainingSpace() - F.getProducts() + B.getProducts());
            secondRoute.setFilledSpace(secondRoute.getFilledSpace() + B.getProducts() - F.getProducts());
            secondRoute.setRemainingSpace(secondRoute.getRemainingSpace() - B.getProducts() + F.getProducts());
            // execute the swap move
            // add customer F from the second to the first route in the firstClientPosition index
            // add customer B from the first to the second route in the secondClientPosition index
            firstRoute.getClientsToVisit().set(sm.getFirstClientPosition(), F);
            secondRoute.getClientsToVisit().set(sm.getSecondClientPosition(), B);

        }
        // calculate the total cost of the solution
        sol.setTotalCost(sol.getTotalCost() + sm.getMoveCost());

    }

    // check if a move is tabu
    private boolean moveIsTabu(Move m) {
        // if the tabu list is empty, no tabu
        if (tabuList.isEmpty()){
            // add the new move in the list
            tabuList.add(m);
            return false;
        }
        // iterate the tabu list
        boolean returnValue = false;
        for(Move tmove : tabuList) {
            // if the move is found in the list 
            // return true - the move is tabu
            if (tmove.isEqual(m)){
                returnValue = true;
                break;
            }
        }
        // add the new move in the list
        tabuList.add(m);
        // remove an element, if needed - check with the max size of the tabu list
        if (tabuList.size() > Utils.TABU_MAX_SIZE) tabuList.poll();
        // return true/ false --> is a tabu move or not?
        return returnValue;
    }

    // check if we have reached a local minimum
    private boolean localOptimumHasBeenReached(int operatorType, RelocationMove rm, SwapMove sm) {
        // check the cost to find out if local minimum is reached
        if (operatorType == 0) {
            return rm.getMoveCost() > -0.00001;
        } else if (operatorType == 1) {
            return sm.getMoveCost() > 0.00001;
        }
        return false;
    }

    // calculate the total cost of the solution
    private void testSolution(Solution solution) {

        double secureSolutionCost = 0;
        for (TruckRoute rt : solution.getRoutes()) {
            double secureRouteCost = 0;

            for (int j = 0; j < rt.getClientsToVisit().size() - 1; j++) {
                VisitLocation A = rt.getClientsToVisit().get(j);
                VisitLocation B = rt.getClientsToVisit().get(j + 1);

                secureRouteCost = secureRouteCost + distanceMatrix.get(A.getClientId()).get(B.getClientId());
            }

            secureSolutionCost = secureSolutionCost + secureRouteCost;
        }
    }
}
