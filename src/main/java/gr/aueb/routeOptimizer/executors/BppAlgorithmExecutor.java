package gr.aueb.routeOptimizer.executors;

import gr.aueb.routeOptimizer.models.Solution;
import gr.aueb.routeOptimizer.models.TruckRoute;
import gr.aueb.routeOptimizer.models.VisitLocation;
import gr.aueb.routeOptimizer.utils.Utils;
import gr.aueb.routeOptimizer.utils.ResultsWriter;
import org.apache.commons.collections4.CollectionUtils;

import java.util.ArrayList;
import java.util.List;

public class BppAlgorithmExecutor {

    // Question 2
    public Solution bppAlgorithm(List<VisitLocation> visitLocations, List<List<Double>> distanceMatrix) {
        System.out.println("============= BPP Algorithm =================");
        List<TruckRoute> usedTruckRoutes = new ArrayList<>();
        // iterate all the customers of the list
        visitLocations.forEach(client -> {
            // starting point is already added when a new route is created
            // assign remaining visiting locations (clients) to the best fitting truck
            if (visitLocations.indexOf(client) != 0) {
                findBestTruck(usedTruckRoutes, client, visitLocations.get(0), distanceMatrix);
            }
        });
        // all trucks should return to the main storage - add starting point at the end of the customer's list in each route
        usedTruckRoutes.forEach(route -> route.getClientsToVisit().add(visitLocations.get(0)));
        // create a new solution object
        Solution solution = new Solution();
        // set as routes the produced list of TruckRoute object
        solution.setRoutes(usedTruckRoutes);
        // calculate the total cost of the produced solution
        Utils.calcTotalCostOfSolution(solution, distanceMatrix);
        System.out.println("BPP algorithm result: " + solution);
        // write the solution in a file in RouteOptimizer folder in json format
        ResultsWriter.writeResults(solution, Utils.BPP_RESULTS_FILE);
        System.out.println("============= Finished BPP Algorithm =================");
        return solution;
    }

    private void findBestTruck(List<TruckRoute> usedTruckRoutes, VisitLocation visitLocation,
                               VisitLocation startingPoint, List<List<Double>> distanceMatrix) {
        TruckRoute bestTruckRoute = new TruckRoute();
        // all trucks should start from the main storage - starting point
        bestTruckRoute.getClientsToVisit().add(startingPoint);
        // flag to check if a fitting route is already in the list usedTruckRoutes
        boolean truckInList = false;
        // check if the list has items
        if(CollectionUtils.isNotEmpty(usedTruckRoutes)) {
            // iterate all routes
            for (TruckRoute truckRoute : usedTruckRoutes) {
                // get the last customer of the list
                VisitLocation from = truckRoute.getClientsToVisit().get(truckRoute.getClientsToVisit().size() - 1);
                // check if the current route is the best fitting route for this customer
                if (truckRoute.isBestTruckForBPP(distanceMatrix, visitLocation, from, bestTruckRoute)) {
                    // change the best route and make flag true
                    bestTruckRoute = truckRoute;
                    truckInList = true;
                }
            }
        }
        // if flag is still false
        if (!truckInList) {
            // add the new route, created in line 40, to the list and generate a new id for the route
            usedTruckRoutes.add(bestTruckRoute);
            bestTruckRoute.setTruckId(usedTruckRoutes.indexOf(bestTruckRoute) + 1);
        }
        // update the fields of the new best route with the customer's data (distance, products etc)
        bestTruckRoute.setRemainingSpace(bestTruckRoute.getRemainingSpace() - visitLocation.getProducts());
        bestTruckRoute.setFilledSpace(bestTruckRoute.getFilledSpace() + visitLocation.getProducts());
        VisitLocation from = bestTruckRoute.getClientsToVisit().get(bestTruckRoute.getClientsToVisit().size() - 1);
        Double costAdded = distanceMatrix.get(from.getClientId()).get(visitLocation.getClientId());
        bestTruckRoute.setTotalTimeNeeded(bestTruckRoute.getNewTotalTimeNeeded(costAdded));
        bestTruckRoute.getClientsToVisit().add(visitLocation);
        visitLocation.setVisited(Boolean.TRUE);
    }

}
