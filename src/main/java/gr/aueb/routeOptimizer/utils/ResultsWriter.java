package gr.aueb.routeOptimizer.utils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import gr.aueb.routeOptimizer.models.Solution;

import java.io.File;
import java.io.PrintWriter;

public class ResultsWriter {

    // Gson is a Google library to handle json
    // setPrettyPrinting is used so as not to write the json in a single line
    private static Gson gson = new GsonBuilder().setPrettyPrinting().create();

    /**
     * Export a solution in json format to a file. All files can be found
     * in RouteOptimizer folder
     * @param solution the produced solution
     * @param fileName the name of the file to export the solution
     */
    public static void writeResults(Solution solution, String fileName) {
        // write solution to file
        String json = gson.toJson(solution, Solution.class);
        try {
            File file = new File(fileName);
            PrintWriter writer = new PrintWriter(file);
            writer.println(json);
            writer.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
