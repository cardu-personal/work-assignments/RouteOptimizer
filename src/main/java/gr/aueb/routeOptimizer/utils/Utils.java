package gr.aueb.routeOptimizer.utils;

import gr.aueb.routeOptimizer.models.Solution;
import gr.aueb.routeOptimizer.models.TruckRoute;
import gr.aueb.routeOptimizer.models.VisitLocation;

import java.util.List;
import java.util.Random;

public class Utils {

    public static final String BPP_RESULTS_FILE = "RouteOptimizer/BppAlgorithmResults.json";
    public static final String LOCAL_SEARCH_RESULTS_FILE = "RouteOptimizer/LocalSearchResults";
    public static final String TABU_RESULTS_IMAGE = "RouteOptimizer/TabuSearchImage";
    public static final String TABU_RESULTS_FILE = "RouteOptimizer/TabuSearchResults.json";
    public static final Integer TABU_REPETITIONS = 10000;
    public static final Integer TABU_MAX_SIZE = 8;
    public static final int OPERATOR_TYPE = new Random().nextInt(1);

    /**
     * Copy a route to a new TruckRoute object
     * @param rt the existing TruckRoute object
     * @return a new object with the same values in its fields
     */
    public static TruckRoute cloneRoute(TruckRoute rt) {
        TruckRoute cloned = new TruckRoute();
        cloned.totalDistance = rt.totalDistance;
        cloned.totalTimeNeeded = rt.totalTimeNeeded;
        cloned.remainingSpace = rt.remainingSpace;
        cloned.filledSpace = rt.filledSpace;
        cloned.truckId = rt.truckId;
        cloned.clientsToVisit.addAll(rt.clientsToVisit);
        return cloned;
    }

    /**
     * Copy a solution to a new Solution object
     * @param sol an existing solution object
     * @return a new Solution object with the same values in its fields
     */
    public static Solution cloneSolution(Solution sol) {
        Solution cloned = new Solution();
        cloned.totalCost = sol.totalCost;
        for (TruckRoute route : sol.routes) {
            TruckRoute clonedRoute = cloneRoute(route);
            cloned.routes.add(clonedRoute);
        }

        return cloned;
    }

    /**
     * Add the cost of all routes to calculate the total cost of the solution
     * @param currentSolution the produced solution
     * @param distanceMatrix the 2D array with the distances between the clients
     */
    public static void calcTotalCostOfSolution(Solution currentSolution, List<List<Double>> distanceMatrix){
        List<TruckRoute> routes = currentSolution.routes;
        Double totalCost = 0.0;
        for(TruckRoute route : routes) {
            route.totalDistance = calCostOfRoute(route, distanceMatrix);
            totalCost = totalCost + route.totalDistance;
        }
        currentSolution.totalCost = totalCost;
    }

    /**
     * Method that calculates the cost of a route based on the 2D distance array
     * @param route
     * @param distanceMatrix
     * @return the cost of the route
     */
    public static Double calCostOfRoute(TruckRoute route, List<List<Double>> distanceMatrix) {
        double totalDistance = 0.0;
        // get the customers in a route
        List<VisitLocation> visitPoints = route.clientsToVisit;
        // get a customer and the previous one (starting from the second item of the list)
        // find their distance in the 2D array and add it to the total cost of the route
        for(int i=1; i< visitPoints.size(); i++) {
            VisitLocation from = visitPoints.get(i-1);
            VisitLocation to = visitPoints.get(i);

            Double costAdded = distanceMatrix.get(from.clientId).get(to.clientId);
            totalDistance += costAdded;
        }
        return totalDistance;
    }

    // update the total time of a route 
    // to calculate the time you need the distance (the velocity is given) and the standard time to
    // serve a client
    public static void updateTotalTime(TruckRoute route, List<List<Double>> distanceMatrix) {
        for(int i =1; i< route.clientsToVisit.size(); i++) {
            VisitLocation from = route.clientsToVisit.get(i-1);
            VisitLocation to = route.clientsToVisit.get(i);
            Double costAdded = distanceMatrix.get(from.clientId).get(to.clientId);
            route.totalTimeNeeded = route.getNewTotalTimeNeeded(costAdded);
        }
    }

}
