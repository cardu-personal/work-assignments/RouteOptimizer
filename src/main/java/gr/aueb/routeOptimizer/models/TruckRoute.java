package gr.aueb.routeOptimizer.models;

import java.io.Serializable;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

// This class represents a Truck which has to execute a route to serve some customers
// Each TruckRoute has an id, a field keeping the remaining and the already filled space of the truck
// remainingSpace + filledSpace <= capacity, totalTimeNeeded should be less than 5 hours, totalDistance
// keeps the totalCost (distance) between the customers of the route and clientsToVisit is a list
// with the customers to be visited
public class TruckRoute implements Serializable {

    public static final LocalTime totalTimeLimit = LocalTime.of(5, 0);
    public static final Integer capacity = 1500;
    public static final Integer velocity = 35;

    private Integer truckId;
    private Integer remainingSpace = 1500;
    private Integer filledSpace = 0;
    private LocalTime totalTimeNeeded;
    private Double totalDistance = 0.0;
    private List<VisitLocation> clientsToVisit = new ArrayList<>();

    // This method is used in the BPP Algorithm
    // Gets the cost between two customers from the 2D distance array (distanceMatrix)
    // Calculates the new total time of the route taking into account the new client to be added
    // Check if this is the best route. 
    public boolean isBestTruckForBPP(List<List<Double>> distanceMatrix, VisitLocation visitLocation, VisitLocation from,
                                     TruckRoute bestTruckRoute) {
        Double costAdded = distanceMatrix.get(from.getClientId()).get(visitLocation.getClientId());
        LocalTime newLocalTime = getNewTotalTimeNeeded(costAdded);
        // check that the filledSpace and the remaining space are less than the capacity of the truck
        // check that the remainingSpace of this route is less than the remaining space of the best route so far
        // check that the remainingSpace is enough for the products of the new customer
        // and check that the total time is less than 5h
        return (((this.filledSpace + this.remainingSpace) <= TruckRoute.capacity) &&
                this.remainingSpace < bestTruckRoute.remainingSpace) &&
                this.remainingSpace >= visitLocation.getProducts() &&
                newLocalTime.isBefore(totalTimeLimit);
    }

    // get the total time of a route by adding the time needed for the new customer
    public LocalTime getNewTotalTimeNeeded(Double distance) {
        // get the time needed to visit a new client based on distance from the previous
        double hour = distance / velocity;
        int minutes = (int) Math.round(hour * 60) + 15;
        int hours = minutes / 60;
        int minutesInt = minutes % 60;
        LocalTime timeNeeded = LocalTime.of(hours, minutesInt);
        if(totalTimeNeeded == null) {
            return timeNeeded;
        } else {
            int totalMinutePart = timeNeeded.getMinute() + totalTimeNeeded.getMinute();
            if(totalMinutePart > 59) {
                totalMinutePart = totalMinutePart - 60;
                return LocalTime.of(timeNeeded.getHour() + 1 + totalTimeNeeded.getHour(), totalMinutePart);
            } else {
                return LocalTime.of(timeNeeded.getHour() + totalTimeNeeded.getHour(), totalMinutePart);
            }

        }
    }

    public TruckRoute() {

    }

    public Integer getTruckId() {
        return truckId;
    }

    public void setTruckId(Integer truckId) {
        this.truckId = truckId;
    }

    public Integer getRemainingSpace() {
        return remainingSpace;
    }

    public void setRemainingSpace(Integer remainingSpace) {
        this.remainingSpace = remainingSpace;
    }

    public Integer getFilledSpace() {
        return filledSpace;
    }

    public void setFilledSpace(Integer filledSpace) {
        this.filledSpace = filledSpace;
    }

    public LocalTime getTotalTimeNeeded() {
        return totalTimeNeeded;
    }

    public void setTotalTimeNeeded(LocalTime totalTimeNeeded) {
        this.totalTimeNeeded = totalTimeNeeded;
    }

    public Double getTotalDistance() {
        return totalDistance;
    }

    public void setTotalDistance(Double totalDistance) {
        this.totalDistance = totalDistance;
    }

    public List<VisitLocation> getClientsToVisit() {
        return clientsToVisit;
    }

    public void setClientsToVisit(List<VisitLocation> clientsToVisit) {
        this.clientsToVisit = clientsToVisit;
    }

    @Override
    public String toString() {
        return "TruckRoute{" +
                "truckId=" + truckId +
                ", remainingSpace=" + remainingSpace +
                ", filledSpace=" + filledSpace +
                ", clientsToVisit=" + clientsToVisit +
                ", totalDistance=" + totalDistance +
                ", totalTimeNeeded=" + totalTimeNeeded+
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TruckRoute that = (TruckRoute) o;
        return Objects.equals(truckId, that.truckId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(truckId);
    }
}
